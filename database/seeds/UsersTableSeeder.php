<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

//    Test account for using the admin section

    public function run()
    {
        DB::table('users')->insert([
            'name' => ('Guest'),
            'email' => ('guest@user.com'),
            'password' => bcrypt('Password')
        ]);

    }
}
