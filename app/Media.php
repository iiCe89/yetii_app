<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    public function user() {
        return $this->hasMany( User::class );
    }

    public function Posts() {
        return $this->hasMany( Post::class, 'id', 'post_id' );
    }

}
