<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $fillable = [
        'slug',
        'post_title',
        'created_by',
        'image',
        'body',
        'media',
    ];

    public function user() {
        return $this->belongsTo( 'App\User', 'user_id', 'id' );
    }

    public function Post() {
        return $this->hasMany( Post::class );
    }

}
