<nav class="navbar navbar-expand-lg navbar-dark">
    <a class="navbar-brand" href="#"><span class="badge badge-pill badge-warning">Admin</span></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <button type="button" id="sidebarCollapse" class="btn btn-info btn-sm">
        <i class="fas fa-align-left"></i>
        <span>Toggle Sidebar</span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
</nav>