@extends('layout.app')

@section('content')
    @include('includes._admin_navigation')
    <div class="wrapper">
        <!-- Sidebar  -->
        @include('includes._side_nav')
        <div id="content">
            <h1>Hello world</h1>
        </div>
    </div>
@endsection