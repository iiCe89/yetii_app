#### Contributing to Yetii guide

If you are new to contibuting to open source projects, please follow the steps below

Fork the repository
Clone the forked repository to your local system

Create a feature branch to make your changes Feat-name_of_feature
Make changes and commit to the branch

Push the branch to Gitlab
Submit Pull request


#### Contributers list

@iiCe89 - Daniel
