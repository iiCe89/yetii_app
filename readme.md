##### Getting started

```
CD file location
git clone https://gitlab.com/iiCe89/yetii_app.git
composer install
npm install

Set up your DB in .env
Run php artisan migrate

Laravel 5.6
Bootstrap 4.0+
scss changes require "npm run dev"

Seed database with default accounts
